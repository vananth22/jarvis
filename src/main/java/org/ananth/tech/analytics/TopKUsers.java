package org.ananth.tech.analytics;

import com.clearspring.analytics.stream.StochasticTopper;

import java.io.Serializable;
import java.util.List;


public class TopKUsers<E> implements Serializable{


    private static int k = 2;
    private StochasticTopper<E> stochasticTopper;

    public TopKUsers() {
        this(2); // default top k element
    }


    public TopKUsers(int givenK) {
        this.k = givenK;
        this.stochasticTopper = new StochasticTopper<E>(200); // sample size defaulted to 200
    }

    public  void add(final E e) {
        if(e == null) {
            return;
        }

        this.stochasticTopper.offer(e);
    }

    public void addList(final List<E> givenList) {
        if(givenList == null || givenList.size() == 0) {
            return;
        }

        for(E e: givenList) {
            this.add(e);
        }
    }


    public List<E> getTopK() {
        return this.stochasticTopper.peek(this.k);

    }


}

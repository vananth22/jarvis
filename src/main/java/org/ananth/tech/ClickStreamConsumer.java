package org.ananth.tech;

import org.ananth.tech.consumer.ClickStreamSparkConsumer;

import java.io.IOException;


/**
 * Kickstart method that initialize spark streaming server
 * in local mode and start consuming messages from kafka
 */

public class ClickStreamConsumer {


    public static void main(String... args) throws IOException {
        final ClickStreamSparkConsumer clickStreamSparkConsumer = new ClickStreamSparkConsumer();
        clickStreamSparkConsumer.consumeSpark();

    }

}

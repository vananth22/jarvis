# Jarvis

> Jarvis is an in memory no-etl stream based prototype. Jarvis spin off the following components in JVM to run through the entire streaming data pipeline.

  - Kafka
  - Zookeeper
  - Spark Streaming
  - LMax Disruptor Service
  
### How it works
  
- The json event string generated from Random Event Generator and pass through via Byte Buffer. 
- Json string then get serialized in to java object, passed via LMax Disruptor Service. 
- The LMax Disruptor Handler act as a producer from Kafka and pushes the java event Object in to Kafka broker. 
- A spark streaming application consume data from Kafka and calculate Top K users using stream lib.

### How to run
- Run ClickStreamMain.java. This will start ingesting data in to Kafka via LMax Disruptor Service
- Run ClickStreamConsumer.java. This will initialize spark streaming application and start consuming data from Kafka and calculate Top K users

### Version
0.1

### TODO
- Code refactor to make little more generic
- Doh!! used Java serialization. Should change to more binary format like protobuff
- Add Elastic search and HBase support as in memory sink
- Add Mqtt support instead of Byte Buffer as an event source
- Add more unit test cases
- Add docker support to create in-memory integration testing framework.
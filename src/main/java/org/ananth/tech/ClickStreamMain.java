package org.ananth.tech;

import com.lmax.disruptor.RingBuffer;
import org.ananth.tech.eventsource.ClickStreamLMaxProducer;
import org.ananth.tech.server.KafkaIntegrationServer;
import org.ananth.tech.server.LMaxService;
import org.ananth.tech.model.ClickStreamEvent;
import org.ananth.tech.util.RandomDataGenerator;

import java.nio.ByteBuffer;


/**
 * Bootstrap class for the framework.
 * Click Stream Main will do the following
 * 1. Initialize local Kafka server
 * 2. Initialize local Zoo Keeper server
 * 3. Initialize LMax Disruptor Service
 * 4. Create Ring Buffer
 * 5. Start generating Click Stream data
 */

public class ClickStreamMain {

    private KafkaIntegrationServer kafkaServer;


    public static void main(String... args) throws InterruptedException {

        ClickStreamMain clickStreamMain = new ClickStreamMain();
        clickStreamMain.init();
        final LMaxService lMaxService = new LMaxService();
        final RingBuffer<ClickStreamEvent> ringBuffer = lMaxService.getRingBuffer();
        generateData(ringBuffer);
    }

    private static void generateData(RingBuffer<ClickStreamEvent> ringBuffer) throws InterruptedException {
        final ClickStreamLMaxProducer clickStreamLMaxProducer = new ClickStreamLMaxProducer(ringBuffer);
        ByteBuffer byteBuffer = ByteBuffer.allocate(2048);
        while (true) {
            byte[] value = RandomDataGenerator.getNextBytes();
            byteBuffer.put(value, 0, value.length);
            clickStreamLMaxProducer.onData(byteBuffer);
           // Thread.sleep(1000);
        }
    }

    private void tearDown() {
        this.kafkaServer.stopServer();

    }

    /**
     * Intialize Kafka and Zookeeper service
     */
    private void init() {
        this.kafkaServer = new KafkaIntegrationServer();
        this.kafkaServer.startServer();
    }

}

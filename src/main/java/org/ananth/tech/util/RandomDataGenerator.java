package org.ananth.tech.util;

import com.google.common.collect.Maps;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Stateless Random Data generator to generate dummy data
 */
public final class RandomDataGenerator {

    private static final String[] USERS = {"user-0", "user-1", "user-2", "user-3", "user-4", "user-5", "user-6", "user-7", "user-8", "user-9"};

    private static final AtomicLong sequence = new AtomicLong(1);

    private RandomDataGenerator() {};


    public static String getNext() {
        return getJsonString();
    }

    public static byte[] getNextBytes() {
        return getNext().getBytes();
    }

    private static Long getSequence() {
        return sequence.getAndIncrement();
    }


    private static String getRandomUser() {
        return USERS[randomInt(0, 9)];
    }

    private static String getEventTime() {
        return String.valueOf(System.currentTimeMillis());
    }


    private static String getJsonString() {
        final ObjectMapper objectMapper = new ObjectMapper();
        String value = null;
        try {
            value = objectMapper.writeValueAsString(doDataMapGenerate());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return value + System.lineSeparator();
    }


    private static int randomInt(int min, int max) {
        final Random random = new Random();
        return random.nextInt((max - min) + 1) + min;

    }


    private static Map<String, String> doDataMapGenerate() {
        Map<String, String> dataMap = Maps.newHashMap();
        dataMap.put("user_id", getRandomUser());
        dataMap.put("timestamp", getEventTime());
        dataMap.put("event_id", String.valueOf(getSequence()));
        return dataMap;
    }

}

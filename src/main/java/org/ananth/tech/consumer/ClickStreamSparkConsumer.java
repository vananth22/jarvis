package org.ananth.tech.consumer;

import com.clearspring.analytics.util.Lists;
import kafka.serializer.StringDecoder;
import org.ananth.tech.analytics.TopKUsers;
import org.ananth.tech.exception.ConsumerException;
import org.ananth.tech.util.ClickStreamDecoder;
import org.ananth.tech.model.ClickStreamEvent;
import org.ananth.tech.util.Constants;
import org.ananth.tech.util.PropertyUtil;
import org.apache.spark.SparkConf;
import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka.KafkaUtils;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class ClickStreamSparkConsumer {




    private Map<String, String> getPropertyMap() {
        try {
            return PropertyUtil.withPropertyFile(Constants.KAFKA_CONSUMER_PROPERTY).buildAsMap();
        } catch (IOException e) {
            throw new ConsumerException(e.getMessage(), e.getCause());
        }
    }


    private Set<String> getTopicList() {
        Set<String> topicList = new HashSet<>();
        topicList.add(Constants.TOPIC);
        return topicList;
    }


    public void consumeSpark() {
        final SparkConf conf = new SparkConf().setAppName("Data Flow Consumer")
                .setMaster("local[*]");
        final JavaStreamingContext javaStreamingContext = new JavaStreamingContext(conf, new Duration(500));
        JavaPairInputDStream<String, ClickStreamEvent> events = KafkaUtils.createDirectStream(javaStreamingContext,
                String.class,
                ClickStreamEvent.class,
                StringDecoder.class,
                ClickStreamDecoder.class,
                this.getPropertyMap(),
                this.getTopicList());

        JavaDStream<ClickStreamEvent> documentJavaDStream = events.map(event -> event._2);
        documentJavaDStream.foreachRDD(documents -> {
            final TopKUsers<String> topKUsers = new TopKUsers<>();
            topKUsers.addList(this.getUserList(documents.collect()));
            System.out.println(topKUsers.getTopK());
        });
        javaStreamingContext.start();
        javaStreamingContext.awaitTermination();

    }



    private List<String> getUserList(List<ClickStreamEvent> events) {

        List<String> userList = Lists.newArrayList();
        if(events == null || events.isEmpty()) {
            return userList;
        }

        for(ClickStreamEvent clickStreamEvent : events) {
            userList.add(clickStreamEvent.getUserId());
        }

        return userList;

    }


}

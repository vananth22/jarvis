package org.ananth.tech.model;

import com.lmax.disruptor.EventFactory;

public class ClickStreamEventFactory implements EventFactory<ClickStreamEvent> {


    @Override
    public ClickStreamEvent newInstance() {
        return new ClickStreamEvent();
    }
}

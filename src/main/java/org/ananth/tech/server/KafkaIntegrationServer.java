package org.ananth.tech.server;


import kafka.server.KafkaConfig;
import kafka.server.KafkaServerStartable;
import org.ananth.tech.exception.ServerException;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

/**
 * Spin off stand alone Kafka server inside JVM
 */


public class KafkaIntegrationServer {


    private KafkaServerStartable kafkaServerStartable;
    private static ZookeeperIntegrationServer zookeeper;
    private KafkaConfig kafkaConfig;
    private Properties properties;

    static {
        try {
            initZookeeper();
        } catch (Exception e) {
            throw new ServerException(e.getMessage(), e.getCause());
        }
    }

    public KafkaIntegrationServer() {
        this(defaultProperties());

    }


    public KafkaIntegrationServer(final Properties properties) {
        this.properties = properties;
        initKafka();

    }

    public static void initZookeeper() {
        try {
            zookeeper = new ZookeeperIntegrationServer();
            zookeeper.startZKServer();
        } catch (Exception e) {
            throw new ServerException(e.getMessage(), e.getCause());
        }

    }


    public void initKafka() {
        try {
            this.properties.setProperty("log.dirs", this.getTempFilePath(this.properties));
            this.kafkaConfig = new KafkaConfig(this.properties);
        } catch (Exception e) {
            throw new ServerException(e.getMessage(), e.getCause());
        }
    }


    public void startServer() {
        this.kafkaServerStartable = new KafkaServerStartable(this.kafkaConfig);
        System.out.println("Kafka server about to start");
        this.kafkaServerStartable.startup();
        System.out.println(this.kafkaConfig.advertisedHostName());
        System.out.println(this.kafkaConfig.advertisedPort());


    }

    public void stopServer() {
        System.out.println("Kafka server about to end");
        this.kafkaServerStartable.shutdown();
        try {
            zookeeper.stopZKServer();
        } catch (IOException e) {
            throw new ServerException(e.getMessage(), e);
        }
    }

    private static Properties defaultProperties() {
        final Properties properties = new Properties();
        properties.put("zookeeper.connect", zookeeper.getZookeeperConnectionParam());
        properties.put("broker.id", "1");
        properties.put("log.dirs", "/tmp");
        properties.put("port", "49123");
        properties.put("advertised.host.name", "127.0.0.1");
        return properties;
    }


    private static String getTempFilePath(final Properties properties) throws IOException {

        if (properties == null || properties.get("log.dirs") == null) {
            throw new IOException("Cant' create temp file: logs.dir property is null or empty");
        }

        String tempDir = properties.get("log.dirs") + "/" + Double.toHexString(Math.random());
        File logDir = new File(tempDir);
        logDir.mkdirs();
        return logDir.getAbsolutePath();

    }


}

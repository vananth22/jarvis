package org.ananth.tech.util;


import com.google.common.collect.Maps;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;

public class PropertyUtil {

    private String propertyfile;

    private PropertyUtil(String propertyfile) {
        this.propertyfile = propertyfile;
    }

    public static PropertyUtil withPropertyFile(String propertyFile) throws IOException {

        if (propertyFile == null) {
            throw new IOException(String.format("Not a valid file name: %s", propertyFile));
        }
        return new PropertyUtil(propertyFile);
    }

    public Map<String, String> buildAsMap() throws IOException {
        return Maps.fromProperties(this.buildAsProperties());
    }


    public Properties buildAsProperties() throws IOException {
        final Properties configProp = new Properties();
        InputStream in = this.getClass().getClassLoader().getResourceAsStream(this.propertyfile);
        if(in == null) {
            throw new IOException("Invalid property file name. property not found");
        }
        configProp.load(in);
        return configProp;
    }


}

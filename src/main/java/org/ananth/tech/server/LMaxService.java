package org.ananth.tech.server;

import com.lmax.disruptor.RingBuffer;
import com.lmax.disruptor.dsl.Disruptor;
import org.ananth.tech.model.ClickStreamEvent;
import org.ananth.tech.model.ClickStreamEventFactory;
import org.ananth.tech.eventsource.ClickStreamLMaxEventHandler;
import org.ananth.tech.exception.ServerException;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;


public class LMaxService {

    private Disruptor<ClickStreamEvent> disruptor;

    private static int bufferSize = 1024;


    public LMaxService() {
        this(bufferSize);
    }

    public LMaxService(int bufferSize) {
        this.bufferSize = bufferSize;
        initService();
    }

    private void initService() {
        final Executor executor = Executors.newCachedThreadPool();
        final ClickStreamEventFactory clickStreamEventFactory = new ClickStreamEventFactory();
        this.disruptor = new Disruptor<>(clickStreamEventFactory, this.bufferSize, executor);
        this.disruptor.handleEventsWith(new ClickStreamLMaxEventHandler());
        this.disruptor.start();
    }


    public RingBuffer<ClickStreamEvent> getRingBuffer() {

        if(this.disruptor == null) {
            throw new ServerException("LMax Service not initialized. Can't obtain RingBuffer");
        }

        final RingBuffer<ClickStreamEvent> ringBuffer = this.disruptor.getRingBuffer();
        return ringBuffer;

    }




}

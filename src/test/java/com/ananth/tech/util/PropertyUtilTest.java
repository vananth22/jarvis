package com.ananth.tech.util;

import org.ananth.tech.util.PropertyUtil;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Map;

/**
 * Created by ananth.durai on 3/25/16.
 */
public class PropertyUtilTest {


    @Test
    public void testWithValidProperty() throws IOException {
        Map<String,String> properties = PropertyUtil.withPropertyFile("kafka.consumer.properties").buildAsMap();
        Assert.assertNotNull(properties);
    }


    @Test(expectedExceptions = IOException.class)
    public void testWithInValidProperty() throws IOException {
        Map<String,String> properties = PropertyUtil.withPropertyFile("invalid.properties").buildAsMap();
    }





}

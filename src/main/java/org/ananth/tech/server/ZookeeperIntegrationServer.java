package org.ananth.tech.server;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.curator.test.TestingServer;

import java.io.IOException;

public class ZookeeperIntegrationServer {


    private TestingServer zkServer;
    private int port = 2181;

    public ZookeeperIntegrationServer() {
    }

    public ZookeeperIntegrationServer(int port) {
        this.port = port;
    }

    public void startZKServer() throws Exception {
        this.zkServer = new TestingServer(port);
        ExponentialBackoffRetry retryPolicy = new ExponentialBackoffRetry(
                1000, 3);
        CuratorFramework zookeeper = CuratorFrameworkFactory.newClient(
                this.getZookeeperConnectionParam(), retryPolicy);
        zookeeper.start();
    }

    public void stopZKServer() throws IOException {
        this.zkServer.stop();
        this.zkServer.close();
    }

    public String getZookeeperConnectionParam() {
        return this.zkServer.getConnectString() + "/kafka";
    }


}

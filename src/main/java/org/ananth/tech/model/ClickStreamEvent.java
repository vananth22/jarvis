package org.ananth.tech.model;

import org.codehaus.jackson.annotate.JsonProperty;

import java.io.Serializable;


public class ClickStreamEvent implements Serializable {

    @JsonProperty("event_id")
    private long eventId;
    @JsonProperty("user_id")
    private String userId;
    @JsonProperty
    private Long timestamp;

    public long getEventId() {
        return eventId;
    }

    public ClickStreamEvent setEventId(long eventId) {
        this.eventId = eventId;
        return this;
    }

    public String getUserId() {
        return userId;
    }

    public ClickStreamEvent setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public ClickStreamEvent setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
        return this;
    }


    public void deepCopy(final ClickStreamEvent otherClickStreamEvent) {
        this.eventId = otherClickStreamEvent.eventId;
        this.userId = otherClickStreamEvent.userId;
        this.timestamp = otherClickStreamEvent.timestamp;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ClickStreamEvent{");
        sb.append("eventId=").append(eventId);
        sb.append(", userId='").append(userId).append('\'');
        sb.append(", timestamp=").append(timestamp);
        sb.append('}');
        return sb.toString();
    }
}

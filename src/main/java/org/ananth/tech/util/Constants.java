package org.ananth.tech.util;

/**
 * Configuration properties
 */
public class Constants {


    public static final String TOPIC = "kafka"; // Kafka Topic name to producer and subscriber interact
    public static final String KAFKA_CONSUMER_PROPERTY = "kafka.consumer.properties";
    public static final String KAFKA_PRODUCER_PROPERTY = "kafka.producer.properties";
}

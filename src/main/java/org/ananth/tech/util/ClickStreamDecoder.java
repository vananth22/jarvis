package org.ananth.tech.util;

import kafka.serializer.Decoder;
import kafka.utils.VerifiableProperties;
import org.ananth.tech.model.ClickStreamEvent;
import org.apache.commons.lang3.SerializationUtils;

/**
 * Created by ananth.durai on 3/25/16.
 *
 * Custom Kafka Decode class to decode ClickStream Object
 *
 */
public class ClickStreamDecoder implements Decoder<ClickStreamEvent> {


    private VerifiableProperties verifiableProperties;

    public ClickStreamDecoder() {}

    public ClickStreamDecoder(final VerifiableProperties verifiableProperties) {
        this.verifiableProperties = verifiableProperties;
    }


    @Override
    public ClickStreamEvent fromBytes(byte[] bytes) {
        ClickStreamEvent clickStreamEvent = (ClickStreamEvent) SerializationUtils.deserialize(bytes);
        return clickStreamEvent;
    }
}

package org.ananth.tech.eventsource;

import com.lmax.disruptor.EventHandler;
import com.lmax.disruptor.LifecycleAware;
import org.ananth.tech.exception.ProducerException;
import org.ananth.tech.model.ClickStreamEvent;
import org.ananth.tech.util.Constants;
import org.ananth.tech.util.PropertyUtil;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.io.IOException;
import java.util.Properties;


/**
 *    LMax Disruptor consumer. ClickStreamLMaxEventHandler will receive data from LMax Ring Buffer
 *    and push data in to Kafka Broker.
 */


public class ClickStreamLMaxEventHandler implements EventHandler<ClickStreamEvent>, LifecycleAware {

    private Producer<String, byte[]> producer;
    public ClickStreamLMaxEventHandler() {
        Properties props = defaultProperties();
        this.producer = new KafkaProducer(props);
    }

    public ClickStreamLMaxEventHandler(final Properties properties) {
        this.producer = new KafkaProducer(properties);
    }


    @Override
    public void onEvent(ClickStreamEvent clickStreamEvent, long sequence, boolean endOfBatch) throws Exception {

        System.out.println("click stream called"); // added for state pinging.

        if (clickStreamEvent == null) {
            return;
        }
        byte[] data = SerializationUtils.serialize(clickStreamEvent);
        this.producer.send(new ProducerRecord(Constants.TOPIC, data));

        System.out.println("successfully written"); // added for state pinging.

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onShutdown() {
        this.producer.close();
    }


    private Properties defaultProperties() {
        try {
            return PropertyUtil.withPropertyFile(Constants.KAFKA_PRODUCER_PROPERTY).buildAsProperties();
        } catch (IOException e) {
            throw new ProducerException(e.getMessage(), e.getCause());
        }

    }


}

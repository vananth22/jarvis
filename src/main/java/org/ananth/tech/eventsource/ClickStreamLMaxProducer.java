package org.ananth.tech.eventsource;


import com.lmax.disruptor.RingBuffer;
import org.ananth.tech.exception.ProducerException;
import org.ananth.tech.model.ClickStreamEvent;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * LMax event producer.
 * Given a Ring Buffer from LMax Disruptor, LMax producer will constantly pushing data
 */

public class ClickStreamLMaxProducer {


    private final RingBuffer<ClickStreamEvent> ringBuffer;

    public ClickStreamLMaxProducer(final RingBuffer<ClickStreamEvent> ringBuffer) {
        this.ringBuffer = ringBuffer;
    }

   /* public void onData(ByteBuffer byteBuffer) {
        long sequence = this.ringBuffer.next();
        try {

            final ClickStreamEvent clickStreamEvent = this.ringBuffer.get(sequence);
           // clickStreamEvent.setValue(byteBuffer.getLong(0));
           // clickStreamEvent.setUserId(Byte.toString(byteBuffer.get(1)));
           // clickStreamEvent.setTimestamp(byteBuffer.getLong(2));

        } finally {
            this.ringBuffer.publish(sequence);
        }
    } */

    public void onData(ByteBuffer byteBuffer) {

        long sequence = this.ringBuffer.next();
        try {

            ClickStreamEvent clickStreamEvent = this.ringBuffer.get(sequence);
            byteBuffer.flip();
            String jsonEventData = this.extractEventData(byteBuffer);
            clickStreamEvent.deepCopy(this.convertClickStreamData(jsonEventData));
            byteBuffer.clear();

        } finally {
            this.ringBuffer.publish(sequence);
        }

    }

    private String extractEventData(ByteBuffer byteBuffer) {
        StringBuilder builder = new StringBuilder();
        String lineSeparator = System.lineSeparator();
        char a;
        while (byteBuffer.hasRemaining() && !lineSeparator.equals(a = (char)byteBuffer.get())) {
            builder.append(a);
        }
        String value = builder.toString();
        builder.setLength(0);
        return value;
    }


    public ClickStreamEvent convertClickStreamData(String eventJson) {
        ObjectMapper mapper = new ObjectMapper();
        ClickStreamEvent clickStreamEvent = null;
        try {
            clickStreamEvent = mapper.readValue(eventJson, ClickStreamEvent.class);
        } catch (IOException e) {
            throw new ProducerException(e.getMessage(), e.getCause());
        }
        return clickStreamEvent;
    }


}
